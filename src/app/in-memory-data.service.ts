import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import {Startup} from './Store-models';
import {Consultant} from './Store-models';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService{

  constructor() { }

  createDb() {
    const john = new Consultant(1, "John", "Doe", "Lorem iput dolor asset");
    const jane = new Consultant(2, "Jane", "Doe", "Lorem iput dolor asset");
    const charle = new Consultant(3, "Charle", "Esteban", "Lorem iput dolor asset");
    const claude =new Consultant(3, "Claude", "Exemple", "Lorem iput dolor asset");
    const consultants = [john, jane, charle, claude];

    const startups = [
      new Startup(1, "Baby-care", "Sante", "Elise doe", 1, "Lorem iput dolor asset", john,  "23 boulevard exemple 4", ),
      new Startup(2, "Air respire", "Sante", "Franck lexemple", 2, "Lorem iput dolor asset", jane),
      new Startup(3, "Massage intellet", "Bien etre", "Antoine lexemple2", 1, "Lorem iput dolor asset", charle),
      new Startup(4, "Sondage auto", "Marketin", "Eric lexemple3", 3, "Lorem iput dolor asset",claude,  "23 boulevard exemple 4")
    ];

    return { startups, consultants} ;
  }

  genId<T extends Startup | Consultant>(table: T[]): number {
    return table.length > 0 ? Math.max(...table.map(t => t.id)) + 1 : 11;
  }
}
