import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showcofondateur'
})
export class CofondateurNumberPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value == 1) {
      return 'Unique';
    } else if (value == 2) {
      return 'Couple';
    } else if (value > 2) {
      return 'Groupes';
    }
    return 'toto' ;
  }

}
