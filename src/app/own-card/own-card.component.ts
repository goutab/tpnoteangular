import {Component, Input, OnInit} from '@angular/core';
import Startup from '../Store-models/Startup';

@Component({
  selector: 'app-own-card',
  templateUrl: './own-card.component.html',
  styleUrls: ['./own-card.component.css']
})
export class OwnCardComponent implements OnInit {

  @Input() element: any;
  constructor() { }

  ngOnInit() {
  }

}
