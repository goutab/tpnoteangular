import { Component, OnInit } from '@angular/core';
import {ConsultantService} from '../services/consultant.service';
import Consultant from '../Store-models/Consultant';

@Component({
  selector: 'app-handle-consultant',
  templateUrl: './handle-consultant.component.html',
  styleUrls: ['./handle-consultant.component.css']
})
export class HandleConsultantComponent implements OnInit {
  Consultants: Consultant[];

  constructor(private consultantService: ConsultantService) { }

  ngOnInit() {
    this.consultantService.fetch_all().subscribe(
      tab => this.Consultants = tab
    )
  }

}
