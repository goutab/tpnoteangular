import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HandleStartupComponent } from './handle-startup/handle-startup.component';
import { HandleConsultantComponent} from './handle-consultant/handle-consultant.component';
import { HomeComponent } from './home/home.component';
import { Page404Component } from './page404/page404.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'startups', component: HandleStartupComponent },
  { path: 'consultants', component: HandleConsultantComponent },
  { path: 'page404', component: Page404Component },
  { path: '**', redirectTo: 'page404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
