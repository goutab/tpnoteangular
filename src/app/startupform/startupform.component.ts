import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import Startup from '../Store-models/Startup';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-startupform',
  templateUrl: './startupform.component.html',
  styleUrls: ['./startupform.component.css']
})
export class StartupformComponent  implements OnInit, OnChanges{

  @Input() startup: Startup;
  @Output() startupChange = new EventEmitter<Startup>();

  startupForm: FormGroup;
  idCtrl: FormControl;
  nomCtrl: FormControl;
  secteurCtrl: FormControl;
  representantCtrl: FormControl;
  nbCofondateurCtrl: FormControl;
  descriptionCtrl: FormControl;
  adresseCtrl: FormControl;
  consultantCtrl: FormControl;

  constructor(public fb: FormBuilder) {
    this.idCtrl = fb.control('');
    this.nomCtrl = fb.control('', [Validators.required, Validators.maxLength(20)]);
    this.secteurCtrl = fb.control('', [Validators.required, Validators.maxLength(10)]);
    this.representantCtrl = fb.control('', [Validators.required, Validators.maxLength(15)]);
    this.nbCofondateurCtrl = fb.control('', [Validators.required, Validators.pattern("[0-9]*")]);
    this.descriptionCtrl = fb.control('', [Validators.required, Validators.maxLength(250)]);
    this.adresseCtrl = fb.control('', [Validators.maxLength(25)]);
    this.consultantCtrl = fb.control('');
    this.startupForm = fb.group({
      id: this.idCtrl,
      nom: this.nomCtrl,
      secteurActivite: this.secteurCtrl,
      representantRepresentant: this.representantCtrl,
      nbreCofondateur: this.nbCofondateurCtrl,
      description: this.descriptionCtrl,
      adresse: this.adresseCtrl,
      consultant: this.consultantCtrl
    });
  }

  ngOnInit () {
    this.startupForm.setValue(this.startup);
    console.log(this.startup);
  }

  ngOnChanges () {
    this.startupForm.setValue(this.startup);
    console.log(this.startup);
  }

  OnSubmit() {
    this.startup = this.startupForm.value;
    this.startupChange.emit(this.startup);
  }

}
