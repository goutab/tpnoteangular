import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Consultant} from '../Store-models';

@Injectable({
  providedIn: 'root'
})
export class ConsultantService {

  constructor(private http: HttpClient) { }

  fetch_all(): Observable<Consultant[]> {
    return this.http.get<Consultant[]>("${baseUrl}/consultants");
  }

  get(id): Observable<Consultant> {
    return this.http.get<Consultant>("${baseUrl}/consultants/" + id);
  }
}
