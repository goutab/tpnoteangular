import { Component, OnInit } from '@angular/core';
import {Startup} from '../Store-models';
import {Consultant} from '../Store-models';
import { StartupService } from 'src/app/services/startup.service';
import { ConsultantService } from 'src/app/services/consultant.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  Startups: Startup[];
  Consultants: Consultant[];

  constructor(private startupService: StartupService, private consultantService: ConsultantService) { }

  ngOnInit() {
    this.startupService.fetch_all().subscribe(
      x => this.Startups = x
    );
    this.consultantService.fetch_all().subscribe(
      x => this.Consultants = x
    );
  }

}
