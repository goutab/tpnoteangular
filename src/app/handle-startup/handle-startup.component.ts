import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ConsultantService} from '../services/consultant.service';
import {StartupService} from '../services/startup.service';

import {Startup} from '../Store-models';
import {Consultant} from '../Store-models';

@Component({
  selector: 'app-handle-startup',
  templateUrl: './handle-startup.component.html',
  styleUrls: ['./handle-startup.component.css']
})
export class HandleStartupComponent implements OnInit {
  Startups: Startup[];
  displayForm: boolean = false;
  startup: Startup =  null;
  constructor(private startupService: StartupService, private consultantService: ConsultantService, public fb: FormBuilder) {
  }

  ngOnInit() {
   this.getListStartup();
  }

  getListStartup(){
    this.startupService.fetch_all().subscribe(
      tab => this.Startups = tab
    );
  }
  // Soit on cache le formulaire, soit on initialise un objet vide pour preparer lajout
  handleAddButton() {
    this.startup = this.startup !== null ? null : new Startup(null, "", "", "", 0, "", new Consultant(0,'','',''));
    ;
  }

  saveStartup(startup) {

    // si l'id nest pas null cest une edition
    if (startup.id !== null) {
      this.startupService.edit(startup).subscribe(
        // Je change lelement dans le tableau en faisant une boucle
        x => this.Startups.map(element => element.id === startup.id ? startup  : element )
      );
      this.getListStartup();
      // sinon ces un ajout
    } else {
      this.consultantService.fetch_all().subscribe({
        next: data => {
          const nbRand = Math.floor(Math.random() * data.length);
          startup.consultant = data[nbRand];
          this.startupService.add(startup).subscribe(
            x => this.Startups.push(x)
          );
        }
      });
    }
    // permet de cacher le formulaire
    this.startup = null ;
  }

// fonction qui gere le clique sur le bouton dedition
  handleEditButton(startup) {
    this.startup = startup;

  }

  // fonction qui gere le clique su rle bouton de supression
  supprimer(startup) {
    this.startupService.delete(startup.id).subscribe(
      result => this.Startups.filter(e => e.id != startup.id)
    );
    this.getListStartup();
  }

  cancel(): void {

  }


}
