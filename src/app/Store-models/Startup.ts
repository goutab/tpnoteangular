import Consultant from './Consultant' ;

export default class Startup {
  id: number;
  nom: string;
  secteurActivite: string;
  representantRepresentant: string;
  nbreCofondateur: number;
  description: string;
  adresse: string;
  consultant: Consultant;

  constructor(id: number, nom: string, secteurActivite: string, representantRepresentant: string, nbreCofondateur: number, description: string,  consultant: Consultant, adresse: string= "") {
    this.id = id;
    this.nom = nom;
    this.secteurActivite = secteurActivite;
    this.representantRepresentant = representantRepresentant;
    this.nbreCofondateur = nbreCofondateur;
    this.description = description;
    this.consultant = consultant;
    this.adresse = adresse;
  }
}
