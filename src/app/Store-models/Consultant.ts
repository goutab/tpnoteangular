
export default class Consultant {
  id: number;
  nom: string;
  prenom: string;
  description: string;

  constructor(id: number, nom: string, prenom: string, description: string) {
    this.id = id;
    this.nom = nom;
    this.prenom = prenom;
    this.description = description;
  }
}
